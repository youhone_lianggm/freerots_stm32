
#ifndef _HAL_UART_H_
#define _HAL_UART_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"

/*!_RXTIMEOUTS -> _T12ms*/
#define _RXTIMEOUTS     12
#define _TXBUFFSIZE     32
#define _RXBUFFSIZE     32

#define Open_UART1
//#define Open_UART2
//#define Open_UART3

#ifndef buartUART_DEBUG_OPEN
	#define buartUART_DEBUG_OPEN	0
#endif

#if defined (Open_UART1)

/***************************************************************
*	UART1_TX   PA9
*	UART1_RX   PA10
****************************************************************/
#define USART1_GPIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART1_GPIO_CLK          RCC_APB2Periph_GPIOA

#define USART1_AFIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART1_AFIO_CLK          RCC_APB2Periph_AFIO

#define USART1_CLK_Cmd		       RCC_APB2PeriphClockCmd
#define USART1_CLK               RCC_APB2Periph_USART1

#define USART1_GPIO_PORT         GPIOA
#define USART1_RxPin             GPIO_Pin_10
#define USART1_TxPin             GPIO_Pin_9

#endif

#if defined (Open_UART2)

#define USART2_GPIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART2_GPIO_CLK          RCC_APB2Periph_GPIOA

#define USART2_AFIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART2_AFIO_CLK          RCC_APB2Periph_AFIO

#define USART2_CLK_Cmd		       RCC_APB1PeriphClockCmd
#define USART2_CLK               RCC_APB1Periph_USART2

#define USART2_GPIO_PORT         GPIOA
#define USART2_RxPin             GPIO_Pin_3
#define USART2_TxPin             GPIO_Pin_2

#endif


#if defined (Open_UART3)

#define USART3_GPIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART3_GPIO_CLK          RCC_APB2Periph_GPIOB

#define USART3_AFIO_Cmd		       RCC_APB2PeriphClockCmd
#define USART3_AFIO_CLK          RCC_APB2Periph_AFIO

#define USART3_CLK_Cmd		       RCC_APB1PeriphClockCmd
#define USART3_CLK               RCC_APB1Periph_USART3

#define USART3_GPIO_PORT         GPIOB
#define USART3_RxPin             GPIO_Pin_11
#define USART3_TxPin             GPIO_Pin_10

#endif

#define	recv_event_found	0x12
#define	send_event_found	0x13


#define MAX_PACKAGE_LEN		512 	 	//数据缓冲区最大长度
#define MAX_RINGBUFFER_LEN	MAX_PACKAGE_LEN  	//环形缓冲区最大长度
#define Max_UartBuf         MAX_PACKAGE_LEN  	//串口数据缓冲区最大长度

__packed	typedef struct{
    uint8_t     Message_Buf[Max_UartBuf];
    uint16_t     Message_Len;
    char  	*rb_head;
    char  	*rb_tail;
    uint8_t     uRxTimeouts;
	uint8_t 	flgURxNewDATA;
} UART_HandleTypeDef;

typedef void (* uart_recv_callback_t)(unsigned char *recv_data,unsigned short recv_data_len);
typedef void (* uart_send_callback_t)(unsigned char state);

void UARTx_Init(void);
void UARTx_Run(void);
void UART2_Send_DATA(uint8_t data);
void UART1_Send_DATA(uint8_t data);
void UART3_Send_DATA(uint8_t data);

uint16_t UART1_SendBuf(uint8_t* pBuff, uint16_t PackLen);
uint16_t UART2_SendBuf(uint8_t* pBuff, uint16_t PackLen);
uint16_t UART3_SendBuf(uint8_t* pBuff, uint16_t PackLen);

int net_start_uart1(uart_recv_callback_t recv_callback);
int net_start_uart2(uart_recv_callback_t recv_callback);
int net_start_uart3(uart_recv_callback_t recv_callback);

extern UART_HandleTypeDef uRX1Buff;
extern UART_HandleTypeDef uRX2Buff;
extern UART_HandleTypeDef uRX3Buff;


#endif /*_HAL_UART_H*/


