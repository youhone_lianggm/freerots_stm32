
#include "bsp_uart.h"
#include "shell.h"

#ifdef __GNUC__
// With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf set to 'Yes') calls __io_putchar()
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/* Private typedef -----------------------------------------------------------*/
typedef enum {FAILED = 1, PASSED = 0} Status;

#if defined (Open_UART1)
	UART_HandleTypeDef uRX1Buff;
	uart_recv_callback_t uart1_recv_callback;
#endif

#if defined (Open_UART2)
	UART_HandleTypeDef uRX2Buff;
	uart_recv_callback_t uart2_recv_callback;
#endif

#if defined (Open_UART3)
	UART_HandleTypeDef uRX3Buff;
	uart_recv_callback_t uart3_recv_callback;
#endif

/*********************************************************************
  * @brief  UART GPIO configuration
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
void UART_GPIO_Init(void)
{
GPIO_InitTypeDef GPIO_InitStructure;

#if defined (Open_UART1)																//配置串口1引脚
    USART1_GPIO_Cmd(USART1_GPIO_CLK, ENABLE);
    USART1_AFIO_Cmd(USART1_AFIO_CLK, ENABLE);
    USART1_CLK_Cmd(USART1_CLK, ENABLE);

    GPIO_InitStructure.GPIO_Pin = USART1_TxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(USART1_GPIO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USART1_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART1_GPIO_PORT, &GPIO_InitStructure);
#endif

#if defined (Open_UART2)															//配置串口2引脚
    USART2_CLK_Cmd(USART2_CLK, ENABLE);
	USART2_GPIO_Cmd(USART2_GPIO_CLK, ENABLE);
    USART2_AFIO_Cmd(USART2_AFIO_CLK, ENABLE);
//	GPIO_PinRemapConfig(GPIO_Remap_USART2,ENABLE);
	
    GPIO_InitStructure.GPIO_Pin = USART2_TxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(USART2_GPIO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USART2_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART2_GPIO_PORT, &GPIO_InitStructure);
#endif
#if defined (Open_UART3)														 //配置串口3引脚
    USART3_GPIO_Cmd(USART3_GPIO_CLK, ENABLE);
    USART3_CLK_Cmd(USART3_CLK, ENABLE);
    USART3_AFIO_Cmd(USART3_AFIO_CLK, ENABLE);
//    GPIO_PinRemapConfig(GPIO_PartialRemap_USART3,ENABLE);

    GPIO_InitStructure.GPIO_Pin = USART3_TxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(USART3_GPIO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = USART3_RxPin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(USART3_GPIO_PORT, &GPIO_InitStructure);
#endif
}
/*********************************************************************
  * @brief  UART configuration
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
void UART_Config(void){
    USART_InitTypeDef USART_InitStructure;
#if defined (Open_UART1)																			//配置串口1
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART1, &USART_InitStructure);

//	USART_Cmd(USART1, ENABLE);
    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART1, ENABLE);
    /* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
    如下语句解决第1个字节无法正确发送出去的问题 */
    USART_ClearFlag(USART1, USART_FLAG_TC); /* 清发送完成标志，Transmission Complete flag */
#endif

#if defined (Open_UART2)															//配置串口2
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART2, &USART_InitStructure);

    USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART2, ENABLE);
    /* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
    如下语句解决第1个字节无法正确发送出去的问题 */
    USART_ClearFlag(USART2, USART_FLAG_TC); /* 清发送完成标志，Transmission Complete flag */
#endif

#if defined (Open_UART3)														//配置串口3
    USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART3, &USART_InitStructure);

    USART_ITConfig(USART3,USART_IT_RXNE,ENABLE);
    USART_Cmd(USART3, ENABLE);

    /* CPU的小缺陷：串口配置好，如果直接Send，则第1个字节发送不出去
    如下语句解决第1个字节无法正确发送出去的问题 */
    USART_ClearFlag(USART3, USART_FLAG_TC); /* 清发送完成标志，Transmission Complete flag */
#endif

}

/*********************************************************************
  * @brief  Initialize the NCIC
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
void NVIC_Configuration(void){
    NVIC_InitTypeDef NVIC_InitStructure;
#if defined (Open_UART1)										//使能串口1中断
    //使能串口中断，并设置优先级
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

#if defined (Open_UART2)									 //使能串口2中断
    //使能串口中断，并设置优先级
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif

#if defined (Open_UART3)								  //使能串口3中断
    //使能串口中断，并设置优先级
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
}

/*********************************************************************
  * @brief  UARTx Send DATA.
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
void UART1_Send_DATA(uint8_t data){
    USART_SendData(USART1,data);
    //Loop until the end of transmission
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

void UART2_Send_DATA(uint8_t data){
    USART_SendData(USART2,data);
    //Loop until the end of transmission
    while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
}

void UART3_Send_DATA(uint8_t data){
    USART_SendData(USART3,data);
    //Loop until the end of transmission
    while (USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
}

/*********************************************************************
  * @brief  UARTx Send DATAs.
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/

//不使用半主机模式, 如果没有这段，则需要在target选项中选择使用USE microLIB
// #if 1
// #pragma import(__use_no_semihosting)
// struct __FILE
// {
// 	int handle;
// };
// FILE __stdout;

// _sys_exit(int x)
// {
// 	x = x;
// }
// #endif

PUTCHAR_PROTOTYPE
//int fputc(int ch, FILE *f)
{
    //Place your implementation of fputc here , e.g. write a character to the USART
    USART_SendData(USART1,(uint8_t)ch);
    //Loop until the end of transmission
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    return ch;
}

/*
uint8_t UART_Read(uint8_t* pBuff, uint8_t len){

    uint8_t ulen=0;
    if( flgURxNewDATA ){
	    if( (uRx_Write==uRx_Read)||(uRxTimeouts==0)||flgURdNextDATA ){
	        setflgURdNextDATA();
            while(len--){
                pBuff[ulen++]=uRxBuff[uRx_Read++];
	            if( uRx_Read >= uRx_End ){
	                uRx_Read = uRx_Start;
	            }
	            if( uRx_Read == uRx_Write ){
	                clrflgURxNewDATA();
	                clrflgURdNextDATA();
	                break;
	            }
	        }
	    }
	}
    return ulen;
}
*/

uint16_t UART1_SendBuf(uint8_t* pBuff, uint16_t PackLen){

    uint16_t ulen=0;

	for(ulen=0; ulen<PackLen; ulen++){
        UART1_Send_DATA(pBuff[ulen]);
    }

    return ulen;
}

uint16_t UART2_SendBuf(uint8_t* pBuff, uint16_t PackLen){

    uint16_t ulen=0;

	for(ulen=0; ulen<PackLen; ulen++){
        UART2_Send_DATA(pBuff[ulen]);
    }

    return ulen;
}

uint16_t UART3_SendBuf(uint8_t* pBuff, uint16_t PackLen){

    uint16_t ulen;

    for(ulen=0; ulen<PackLen; ulen++){
        UART3_Send_DATA(pBuff[ulen]);
    }
    return ulen;
}
/*********************************************************************
  * @brief  UARTx Recv DATAs.
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
/**
  * USART1_IRQHandler
  */
#if defined (Open_UART1)
	void USART1_IRQHandler(void){
	  uint8_t value = 0;
	  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET){
			USART_ClearITPendingBit(USART1,USART_IT_RXNE);
			value = USART_ReceiveData(USART1);
			uRX1Buff.Message_Buf[uRX1Buff.Message_Len++] =value;      //uint8_t_REC_DATA;
			fill_rec_buf(value);
			if( uRX1Buff.Message_Len >= (Max_UartBuf-1) ){
				uRX1Buff.Message_Len = 0;
				return;
			}
			uRX1Buff.uRxTimeouts = _RXTIMEOUTS;
			uRX1Buff.flgURxNewDATA = 1;
	#if ( buartUART_DEBUG_OPEN == 1 )
			USART_SendData(USART1,USART_ReceiveData(USART1));
			while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
	#endif // DEBUG_OPEN
		}
	}
#endif

/**
  * USART2_IRQHandler
  */
#if defined (Open_UART2)
	void USART2_IRQHandler(void){
		uint8_t value = 0;
		if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){
			USART_ClearITPendingBit(USART2,USART_IT_RXNE);
			value = USART_ReceiveData(USART2);
			uRX2Buff.Message_Buf[uRX2Buff.Message_Len++] =value;      //uint8_t_REC_DATA;
			if( uRX2Buff.Message_Len >= (Max_UartBuf-1) ){
				uRX2Buff.Message_Len = 0;
				return;
			}
			uRX2Buff.uRxTimeouts = _RXTIMEOUTS;
			uRX2Buff.flgURxNewDATA = 1;

	#if ( buartUART_DEBUG_OPEN == 1 )
			USART_SendData(USART2,USART_ReceiveData(USART2));
			while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	#endif // DEBUG_OPEN
		}
	}
#endif
	
/**
  * USART3_IRQHandler
  */
#if defined (Open_UART3)
	void USART3_IRQHandler(void){
		uint8_t value = 0;
		if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET){
			USART_ClearITPendingBit(USART3,USART_IT_RXNE);
			value = USART_ReceiveData(USART3);
			uRX3Buff.Message_Buf[uRX3Buff.Message_Len++] =value;
			if(uRX3Buff.Message_Len >  (Max_UartBuf-1))
			{
				uRX3Buff.Message_Len = 0;
			}
			uRX3Buff.uRxTimeouts = 10;
			uRX3Buff.flgURxNewDATA = 1;

	#if ( buartUART_DEBUG_OPEN == 1 )
			USART_SendData(USART3,USART_ReceiveData(USART3));
			while (USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
	#endif
		}
	}
#endif

#if defined (Open_UART1)
	int net_start_uart1(uart_recv_callback_t recv_callback)
	{
		uart1_recv_callback = recv_callback;
	#if ( buartUART_DEBUG_OPEN == 1 )
		printf("start uart1 success!\n");
	#endif
		return PASSED;
	}
#endif

#if defined (Open_UART2)
	int net_start_uart2(uart_recv_callback_t recv_callback)
	{
		uart2_recv_callback = recv_callback;
	#if ( buartUART_DEBUG_OPEN == 1 )
		printf("start uart2 success!\n");
	#endif
		return PASSED;
	}
#endif

#if defined (Open_UART3)
int net_start_uart3(uart_recv_callback_t recv_callback)
{
    uart3_recv_callback = recv_callback;
#if ( buartUART_DEBUG_OPEN == 1 )
    printf("start uart3 success!\n");
#endif
    return PASSED;
}
#endif

/*********************************************************************
  * @brief  Initializes the UART peripheral.
	* @param  None
  * @retval None
	* @date   20141205
***********************************************************************/
void UARTx_Init(void){
    UART_GPIO_Init();				//初始化串口引脚
    UART_Config();					//配置串口
    NVIC_Configuration();		//中断配置
}


void UARTx_Run(void){

#if defined (Open_UART1)
    if( (uRX1Buff.uRxTimeouts == 0) && (uRX1Buff.flgURxNewDATA) && (uRX1Buff.Message_Len > 3) ){
        uRX1Buff.flgURxNewDATA = 0;
        if(uart1_recv_callback != NULL){
			uart1_recv_callback(uRX1Buff.Message_Buf, uRX1Buff.Message_Len);
		}
        memset(uRX1Buff.Message_Buf,0,Max_UartBuf);
        uRX1Buff.Message_Len = 0;
	}
#endif
#if defined (Open_UART2)
	if( (uRX2Buff.uRxTimeouts == 0) && (uRX2Buff.flgURxNewDATA) && (uRX2Buff.Message_Len > 3) ){
        uRX2Buff.flgURxNewDATA = 0;
		if(uart2_recv_callback != NULL){
			uart2_recv_callback(uRX2Buff.Message_Buf, uRX2Buff.Message_Len);
		}
        memset(uRX2Buff.Message_Buf,0,Max_UartBuf);
        uRX2Buff.Message_Len = 0;
	}
#endif
#if defined (Open_UART3)
	if( (uRX3Buff.uRxTimeouts == 0) && (uRX3Buff.flgURxNewDATA) && (uRX3Buff.Message_Len > 3) ){
        uRX3Buff.flgURxNewDATA = 0;
		if( uart3_recv_callback != NULL){
			uart3_recv_callback(uRX3Buff.Message_Buf, uRX3Buff.Message_Len);
		}
        memset(uRX3Buff.Message_Buf,0,Max_UartBuf);
        uRX3Buff.Message_Len = 0;
	}
#endif
}
