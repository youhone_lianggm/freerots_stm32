/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   测试led
  ******************************************************************************
  */ 
	
#include "stm32f10x.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "list.h"
#include "portable.h"
#include "timers.h"
#include "bsp_led.h"
#include "bsp_uart.h"
#include "shell.h"

#define NUM_TIMERS 3

TimerHandle_t xTimers[ NUM_TIMERS ];

void vTask(void *pvParameters)
{
	for( ; ;)
	{
		;
	}
}

void vTimerCallback( TimerHandle_t xTimer )
{
const uint32_t ulMaxExpiryCountBeforeStopping = 10;
uint32_t ulCount;
	
	if( xTimer == xTimers[0] ){
		ulCount = ( uint32_t ) pvTimerGetTimerID( xTimers[0] );
		ulCount++;
		if( ulCount >= ulMaxExpiryCountBeforeStopping )
		{
			xTimerStop( xTimers[0], 0 );
		}
		else 
		{
			vTimerSetTimerID( xTimers[0], ( void * ) ulCount );
		}
		macLED1_TOGGLE();
	}
	if( xTimer == xTimers[1] ){
		macLED2_TOGGLE();
	}
	if( xTimer == xTimers[2] ){
		macLED3_TOGGLE();
	}
}

/**
  * @brief  主函数
  * @param  无  
  * @retval 无
  */
int main ( void )
{
long x;
	
	LED_Init();	          /*初始化 LED*/
	UARTx_Init();
	
	xTaskCreate(vTask,"Task1",100,NULL,1,NULL);  
	
	for( x = 0; x < NUM_TIMERS; x++ )
	{
		xTimers[ x ] = xTimerCreate("Timer",( 1000 * x ) + 500,pdTRUE,( void * ) 0,vTimerCallback );
		if( xTimers[ x ] == NULL )
		{
		/* The timer was not created. */
		}
		else
		{
		/* Start the timer. No block time is specified, and even if one was it would be
		ignored because the RTOS scheduler has not yet been started. */
			if( xTimerStart( xTimers[ x ], 0 ) != pdPASS )
			{
			/* The timer could not be set into the Active state. */
			}
		}
	}
	/* ...
	Create tasks here.
	... */
	
	vAnotherFunction();
	
	/* Starting the RTOS scheduler will start the timers running as they have already been set
	into the active state. */
	vTaskStartScheduler();
	/* Should not reach here. */
	for( ;; );

}

/*********************************************END OF FILE**********************/
