#ifndef SHELL_H_INCLUDED
#define SHELL_H_INCLUDED

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"

/* Type definitions. */
#define portCHAR		char
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		long
#define portSHORT		short
#define portSTACK_TYPE	uint32_t
#define portBASE_TYPE	long

typedef portSTACK_TYPE StackType_t;
typedef long BaseType_t;
typedef unsigned long UBaseType_t;

#define pdFALSE			( ( BaseType_t ) 0 )
#define pdTRUE			( ( BaseType_t ) 1 )



#define MY_DEBUG

#ifdef MY_DEBUG
	#define MY_DEBUGF(message) do { \
	                                  {UARTprintf (message);} \
	                               } while(0)
#else
	#define MY_DEBUGF(message)
#endif /* PLC_DEBUG */

extern TaskHandle_t xCmdAnalyzeHandle;

void vTaskCmdAnalyze( void *pvParameters );
void vAnotherFunction( void );
void fill_rec_buf(char data);
								   
#endif // SHELL_H_INCLUDED
