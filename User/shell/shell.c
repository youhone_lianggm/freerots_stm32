
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "shell.h"
#include <ctype.h>
#include "bsp_uart.h"

typedef struct {
	char const *cmd_name;                        //命令字符串
	int32_t max_args;                            //最大参数数目
	void (*handle)(int argc,void * cmd_arg);     //命令回调函数
	char  *help;                                 //帮助信息
}cmd_list_struct;

static uint8_t recv_flag = 0;

#define ARG_NUM     8          //命令中允许的参数个数
#define CMD_LEN     20         //命令名占用的最大字符长度
#define CMD_BUF_LEN 60         //命令缓存的最大长度

typedef struct {
    char rec_buf[CMD_BUF_LEN];            //接收命令缓冲区
    char processed_buf[CMD_BUF_LEN];      //存储加工后的命令(去除控制字符)
    int32_t cmd_arg[ARG_NUM];             //保存命令的参数
}cmd_analyze_struct;

cmd_analyze_struct cmd_analyze;
TaskHandle_t xCmdAnalyzeHandle;

#include <stdarg.h>               /*支持函数接收不定量参数*/
	const char * const g_pcHex = "0123456789abcdef";

/**
* 简介:   一个简单的printf函数,支持\%c, \%d, \%p, \%s, \%u,\%x, and \%X.
*/
void UARTprintf(uint8_t *pcString, ...)
{
	uint32_t ulIdx;
	uint32_t ulValue;       //保存从不定量参数堆栈中取出的数值型变量
	uint32_t ulPos, ulCount;
	uint32_t ulBase;        //保存进制基数,如十进制则为10,十六进制数则为16
	uint32_t ulNeg;         //为1表示从变量为负数
	uint8_t *pcStr;         //保存从不定量参数堆栈中取出的字符型变量
	uint8_t pcBuf[32];      //保存数值型变量字符化后的字符
	uint8_t cFill;          //'%08x'->不足8个字符用'0'填充,cFill='0';
							//'%8x '->不足8个字符用空格填充,cFill=' '
	va_list vaArgP;

	va_start(vaArgP, pcString);
	while(*pcString)
	{
		// 首先搜寻非%核字符串结束字符
		for(ulIdx = 0; (pcString[ulIdx] != '%') && (pcString[ulIdx] != '\0'); ulIdx++)
		{ }
		printf("%.*s",ulIdx,pcString);

		pcString += ulIdx;
		if(*pcString == '%')
		{
			pcString++;

			ulCount = 0;
			cFill = ' ';
again:
			switch(*pcString++)
			{
				case '0': case '1': case '2': case '3': case '4':
				case '5': case '6': case '7': case '8': case '9':
				{
					// 如果第一个数字为0, 则使用0做填充,则用空格填充)
					if((pcString[-1] == '0') && (ulCount == 0))
					{
						cFill = '0';
					}
					ulCount *= 10;
					ulCount += pcString[-1] - '0';
					goto again;
				}
				case 'c':
				{
					ulValue = va_arg(vaArgP, unsigned long);
					UART1_SendBuf((unsigned char *)&ulValue, 1);
					break;
				}
				case 'd':
				{
					ulValue = va_arg(vaArgP, unsigned long);
					ulPos = 0;

					if((long)ulValue < 0)
					{
						ulValue = -(long)ulValue;
						ulNeg = 1;
					}
					else
					{
						ulNeg = 0;
					}
					ulBase = 10;
					goto convert;
				}
				case 's':
				{
					pcStr = va_arg(vaArgP, unsigned char *);

					for(ulIdx = 0; pcStr[ulIdx] != '\0'; ulIdx++)
					{
					}
					UART1_SendBuf(pcStr, ulIdx);

				if(ulCount > ulIdx)
					{
						ulCount -= ulIdx;
						while(ulCount--)
						{
							UART1_SendBuf(" ", 1);
						}
					}
					break;
				}
				case 'u':
				{
					ulValue = va_arg(vaArgP, unsigned long);
					ulPos = 0;
					ulBase = 10;
					ulNeg = 0;
					goto convert;
				}
				case 'x': case 'X': case 'p':
				{
					ulValue = va_arg(vaArgP, unsigned long);
					ulPos = 0;
					ulBase = 16;
					ulNeg = 0;
		 convert:   //将数值转换成字符
					for(ulIdx = 1; (((ulIdx * ulBase) <= ulValue) &&(((ulIdx * ulBase) / ulBase) == ulIdx)); ulIdx *= ulBase, ulCount--)
					{ }
					if(ulNeg)
					{
						ulCount--;
					}
					if(ulNeg && (cFill == '0'))
					{
						pcBuf[ulPos++] = '-';
						ulNeg = 0;
					}
					if((ulCount > 1) && (ulCount < 16))
					{
						for(ulCount--; ulCount; ulCount--)
						{
							pcBuf[ulPos++] = cFill;
						}
					}

					if(ulNeg)
					{
						pcBuf[ulPos++] = '-';
					}

					for(; ulIdx; ulIdx /= ulBase)
					{
						pcBuf[ulPos++] = g_pcHex[(ulValue / ulIdx) % ulBase];
					}
					UART1_SendBuf(pcBuf, ulPos);
					break;
				}
				case '%':
				{
					UART1_SendBuf((pcString - 1), 1);
					break;
				}
				default:
				{
					UART1_SendBuf("ERROR", 5);
					break;
				}
			}
		}
	}
	//可变参数处理结束
	va_end(vaArgP);
}

void fill_rec_buf(char data)
{

    static uint32_t rec_count=0;
   
   cmd_analyze.rec_buf[rec_count]=data;
    if(0x0A==cmd_analyze.rec_buf[rec_count] && 0x0D==cmd_analyze.rec_buf[rec_count-1])
    {
       BaseType_t xHigherPriorityTaskWoken = pdFALSE;
       rec_count=0;
       
       vTaskNotifyGiveFromISR (xCmdAnalyzeHandle,&xHigherPriorityTaskWoken);
       
       portYIELD_FROM_ISR(xHigherPriorityTaskWoken );
    }
    else
    {
       rec_count++;
       
       if(rec_count>=CMD_BUF_LEN)
       {
           rec_count=0;
       }
    }    
}
/**
* 使用SecureCRT串口收发工具,在发送的字符流中可能带有不需要的字符以及控制字符,
* 比如退格键,左右移动键等等,在使用命令行工具解析字符流之前,需要将这些无用字符以
* 及控制字符去除掉.
* 支持的控制字符有:
*   上移:1B 5B 41
*   下移:1B 5B 42
*   右移:1B 5B 43
*   左移:1B 5B 44
*   回车换行:0D 0A
*  Backspace:08
*  Delete:7F
*/
static uint32_t get_true_char_stream(char *dest,const char *src)
{
   uint32_t dest_count=0;
   uint32_t src_count=0;

    while(src[src_count]!=0x0d && src[src_count+1]!=0x0a)
    {
       if(isprint(src[src_count]))
       {
           dest[dest_count++]=src[src_count++];
       }
       else
       {
           switch(src[src_count])
           {
                case    0x08:                          //退格键键值
                {
                    if(dest_count>0)
                    {
                        dest_count --;
                    }
                    src_count ++;
                }break;
                case    0x1B:
                {
                    if(src[src_count+1]==0x5B)
                    {
                        if(src[src_count+2]==0x41 || src[src_count+2]==0x42)
                        {
                            src_count +=3;              //上移和下移键键值
                        }
                        else if(src[src_count+2]==0x43)
                        {
                            dest_count++;               //右移键键值
                            src_count+=3;
                        }
                        else if(src[src_count+2]==0x44)
                        {
                            if(dest_count >0)           //左移键键值
                            {
                                dest_count --;
                            }
                           src_count +=3;
                        }
                        else
                        {
                            src_count +=3;
                        }
                    }
                    else
                    {
                        src_count ++;
                    }
                }break;
                default:
                {
                    src_count++;
                }break;
           }
       }
    }
   dest[dest_count++]=src[src_count++];
    dest[dest_count++]=src[src_count++];
    return dest_count;
}

/*字符串转10/16进制数*/
static int32_t string_to_dec(uint8_t *buf,uint32_t len)
{
   uint32_t i=0;
   uint32_t base=10;       //基数
   int32_t  neg=1;         //表示正负,1=正数
   int32_t  result=0;

    if((buf[0]=='0')&&(buf[1]=='x'))
    {
       base=16;
       neg=1;
       i=2;
    }
    else if(buf[0]=='-')
    {
       base=10;
       neg=-1;
       i=1;
    }
    for(;i<len;i++)
    {
       if(buf[i]==0x20 || buf[i]==0x0D)    //为空格
       {
           break;
       }

       result *= base;
       if(isdigit(buf[i]))                 //是否为0~9
       {
           result += buf[i]-'0';
       }
       else if(isxdigit(buf[i]))           //是否为a~f或者A~F
       {
            result+=tolower(buf[i])-87;
       }
       else
       {
           result += buf[i]-'0';
       }
    }
   result *= neg;

    return result ;
}

/**
* 命令参数分析函数,以空格作为一个参数结束,支持输入十六进制数(如:0x15),支持输入负数(如-15)
* @param rec_buf   命令参数缓存区
* @param len       命令的最大可能长度
* @return -1:       参数个数过多,其它:参数个数
*/
static int32_t cmd_arg_analyze(char *rec_buf,unsigned int len)
{
   uint32_t i;
   uint32_t blank_space_flag=0;    //空格标志
   uint32_t arg_num=0;             //参数数目
   uint32_t index[ARG_NUM];        //有效参数首个数字的数组索引

    /*先做一遍分析,找出参数的数目,以及参数段的首个数字所在rec_buf数组中的下标*/
    for(i=0;i<len;i++)
    {
       if(rec_buf[i]==0x20)        //为空格
       {
           blank_space_flag=1;
           continue;
       }
        else if(rec_buf[i]==0x0D)   //换行
       {
           break;
       }
       else
       {
           if(blank_space_flag==1)
           {
                blank_space_flag=0;
                if(arg_num < ARG_NUM)
                {
                   index[arg_num]=i;
                    arg_num++;
                }
                else
                {
                    return -1;      //参数个数太多
                }
           }
       }
    }

    for(i=0;i<arg_num;i++)
    {
        cmd_analyze.cmd_arg[i]=string_to_dec((unsigned char *)(rec_buf+index[i]),len-index[i]);
    }
    return arg_num;
}

/*打印字符串:Hello world!*/
void printf_hello(int32_t argc,void *cmd_arg)
{
//   MY_DEBUGF(CMD_LINE_DEBUG,("Hello world!\n"));
   do
   {
		{printf ("Hello world!\n");}
   } while(0);
}

/*打印每个参数*/
void handle_arg(int32_t argc,void * cmd_arg)
{
   uint32_t i;
   int32_t  *arg=(int32_t *)cmd_arg;

    if(argc==0)
    {
       printf("无参数\n");
    }
    else
    {
       for(i=0;i<argc;i++)
       {
           printf("第%d个参数:%d\n",i+1,arg[i]);
       }
    }
}


/*命令表*/
const cmd_list_struct cmd_list[]={
/*   命令    参数数目    处理函数        帮助信息                         */
{"hello",   0,      printf_hello,   "hello                      -打印HelloWorld!"},
{"arg",     8,      handle_arg,      "arg<arg1> <arg2> ...      -测试用,打印输入的参数"},
};

/*命令行分析任务*/
void vTaskCmdAnalyze( void *pvParameters )
{
uint32_t i;
int32_t rec_arg_num;
char cmd_buf[CMD_LEN];

	while(1)
	{
	uint32_t rec_num;

		ulTaskNotifyTake(pdTRUE,portMAX_DELAY);  
		printf("recv_flag\r\n");
		rec_num=get_true_char_stream(cmd_analyze.processed_buf,cmd_analyze.rec_buf);

		printf("get_true_char_stream\r\n");
		
	   /*从接收数据中提取命令*/
	   for(i=0;i<CMD_LEN;i++)
	   {
		   if((i>0)&&((cmd_analyze.processed_buf[i]==' ')||(cmd_analyze.processed_buf[i]== 0x0d)))
		   {
				cmd_buf[i]='\0';        //字符串结束符
				break;
		   }
		   else
		   {
				cmd_buf[i]=cmd_analyze.processed_buf[i];
		   }
	   }

	   rec_arg_num=cmd_arg_analyze(&cmd_analyze.processed_buf[i],rec_num);

	   for(i=0;i<sizeof(cmd_list)/sizeof(cmd_list[0]);i++)
	   {
		   if(!strcmp(cmd_buf,cmd_list[i].cmd_name))       //字符串相等
		   {
				if(rec_arg_num<0 || rec_arg_num>cmd_list[i].max_args)
				{
					printf(("参数数目过多!\n"));
				}
				else
				{
					cmd_list[i].handle(rec_arg_num,(void *)cmd_analyze.cmd_arg);
				}
				break;
		   }

	   }
	   if(i>=sizeof(cmd_list)/sizeof(cmd_list[0]))
	   {
		   printf(("不支持的指令!\n"));
	   }
    }
}


void vAnotherFunction( void )
{
/* Create the task. */
	if( xTaskCreate(vTaskCmdAnalyze, "Demo task", 500, NULL, 1, &xCmdAnalyzeHandle) != pdPASS )
	{
		;
	}
	else
	{
		vTaskPrioritySet( xCmdAnalyzeHandle, 2 );
	}
}



